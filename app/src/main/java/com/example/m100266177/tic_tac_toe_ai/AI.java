package com.example.m100266177.tic_tac_toe_ai;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 100266177 on 21/10/2015.
 *
 * Using Resources
 * Dong Xiang, "Solve Tic Tac Toe with the MiniMax algorithm" @ http://www.codeproject.com/Articles/43622/Solve-Tic-Tac-Toe-with-the-MiniMax-algorithm.
 *
 * http://www3.ntu.edu.sg/home/ehchua/programming/java/javagame_tictactoe_ai.html
 * minmax algorithm
 * minimax(level, player)  // player may be "computer" or "opponent"
 * if (gameover || level == 0)
 * return score
 * children = all legal moves for this player
 * if (player is computer, i.e., maximizing player)
 * // find max
 * bestScore = -inf
 * for each child
 * score = minimax(level - 1, opponent)
 * if (score > bestScore) bestScore = score
 * return bestScore
 * else (player is opponent, i.e., minimizing player)
 * // find min
 * bestScore = +inf
 * for each child
 * score = minimax(level - 1, computer)
 * if (score < bestScore) bestScore = score
 * return bestScore
 *
 * // Initial Call
 * minimax(2, computer)
 */
public class AI {

    protected int[][] cells;
    protected int playerOwns = 1;
    protected int computerOwns = -1;
    protected List<int[]> linesToEvaluate = new ArrayList<int[]>();

    public AI(int[][] board) {
        cells = board;

        //Lines preloaded for heuristic evaluation calculation
        linesToEvaluate.add(new int[] {0,0,0,1,0,2});//rows
        linesToEvaluate.add(new int[] {1,0,1,1,1,2});
        linesToEvaluate.add(new int[] {2,0,2,1,2,2});
        linesToEvaluate.add(new int[] {0,0,1,0,2,0});//cols
        linesToEvaluate.add(new int[] {0,1,1,1,2,1});
        linesToEvaluate.add(new int[] {0,2,1,2,2,2});
        linesToEvaluate.add(new int[] {0,0,1,1,2,2});//diagonals
        linesToEvaluate.add(new int[] {0,2,1,1,2,0});
    }


    //Determine AI's move, depth max
    public int[] generateMove(int[][] board) {


        cells = board;

        //int[] result = minimax(2, computerOwns); //TODO remove different starts
        //int[] result = negamax(2, computerOwns);
        //int[] result = minimaxBetaP(2, computerOwns, Integer.MIN_VALUE, Integer.MAX_VALUE);
        int[] result = negamaxBetaP(2, computerOwns, Integer.MIN_VALUE, Integer.MAX_VALUE);

        //Log.i("ERROR", "" + result[0]);//TODO remove debug stuff
        //Log.i("ERROR", "" + result[1]);
        //Log.i("ERROR", "" + result[2]);
        return new int[] {result[0], result[1]};

    }


    //Recursive, Generate minmax tree, binary tree depth 2, current player's turn
    private int[] minimax(int depth, int player) {

        //Create list for all possible moves on board
        List<int[]> moves = new ArrayList<int[]>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (cells[i][j] == 0) {
                    moves.add(new int[] {i, j});
                }
            }
        }

        //init return variables
        int bestValue = 0;
        int currentValue =0;
        int[] bestMove = {-1,-1};

        //maximize computer player while minimizing player/opponent
        if(player == computerOwns)
            bestValue = Integer.MIN_VALUE;
        else
            bestValue = Integer.MAX_VALUE;


        //calculate score for depth = 0
        if(moves.isEmpty() || depth == 0){
            bestValue = 0;
            for(int i = 0; i < linesToEvaluate.size(); i++) {
                bestValue += heuristicEvaluation(linesToEvaluate.get(i), player);
            }
            //Log.i("ERROR", "" + bestValue); //TODO remove DEBug stuff
        }
        else //recursive depth
        {
            int[] temp = {-1,-1};
            for(int i = 0; i < moves.size(); i++){ //try each move
                temp = moves.get(i);
                cells[temp[0]][temp[1]] = player;
                if (player == computerOwns) {  
                    currentValue = minimax(depth - 1, playerOwns)[2];
                    if (currentValue > bestValue) {
                        bestValue = currentValue;
                        bestMove = temp;
                    }
                } else {  
                    currentValue = minimax(depth - 1, computerOwns)[2];
                    if (currentValue < bestValue) {
                        bestValue = currentValue;
                        bestMove = temp;
                    }
                }
                // Reset Move
                cells[temp[0]][temp[1]] = 0;
            }

        }

        return new int[] {bestMove[0], bestMove[1], bestValue};//TODO move best value to end
    }

    //Recursive, Generate minmax tree, binary tree depth 2, current player's turn
    private int[] negamax(int depth, int player) {

        //Create list for all possible moves on board
        List<int[]> moves = new ArrayList<int[]>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (cells[i][j] == 0) {
                    moves.add(new int[] {i, j});
                }
            }
        }

        //init return variables
        int bestValue = Integer.MIN_VALUE;
        int currentValue =0;
        int[] bestMove = {-1,-1};

        //calculate score for depth = 0
        if(moves.isEmpty() || depth == 0){
            bestValue = 0;
            for(int i = 0; i < linesToEvaluate.size(); i++) {
                bestValue += heuristicEvaluation(linesToEvaluate.get(i), player);
            }
            //Log.i("ERROR", "" + bestValue); //TODO remove DEBug stuff
        }
        else //recursive depth
        {
            int[] temp = {-1,-1};
            for(int i = 0; i < moves.size(); i++){ //try each move
                temp = moves.get(i);
                cells[temp[0]][temp[1]] = player;
                currentValue = -negamax(depth -1, -player)[2]; //negamax on player
                if(currentValue > bestValue) {
                    bestValue = currentValue;
                    bestMove = temp;
                }
                // Reset Move
                cells[temp[0]][temp[1]] = 0;
            }

        }

        return new int[] {bestMove[0], bestMove[1], bestValue};//TODO move best value to end
    }

    //Beta Pruning minmax function
    int[] minimaxBetaP(int depth, int player, int alpha, int beta) {

        //Create list for all possible moves on board
        List<int[]> moves = new ArrayList<int[]>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (cells[i][j] == 0) {
                    moves.add(new int[] {i, j});
                }
            }
        }

        //init return variables
        int bestValue = 0;
        int[] bestMove = {-1,-1};


        //calculate score for depth = 0
        if(moves.isEmpty() || depth == 0){
            bestValue = 0;
            for(int i = 0; i < linesToEvaluate.size(); i++) {
                bestValue += heuristicEvaluation(linesToEvaluate.get(i), player);
            }
            return new int[] {bestMove[0], bestMove[1], bestValue};
            //Log.i("ERROR", "" + bestValue); //TODO remove DEBug stuff
        }
        else //recursive depth
        {
            int[] temp = {-1,-1};
            for(int i = 0; i < moves.size(); i++){ //try each move
                temp = moves.get(i);
                cells[temp[0]][temp[1]] = player;
                if (player == computerOwns) {
                    bestValue = minimaxBetaP(depth - 1, playerOwns, alpha, beta)[2];
                    if (bestValue > alpha) {
                        alpha = bestValue;
                        bestMove = temp;
                    }
                } else {
                    bestValue = minimaxBetaP(depth - 1, computerOwns, alpha, beta)[2];
                    if (bestValue < beta) {
                        beta = bestValue;
                        bestMove = temp;
                    }
                }
                // Reset Move
                cells[temp[0]][temp[1]] = 0;

                if (alpha >= beta)
                {
                    break;
                }
            }

        }


        if(player == computerOwns)
            return new int[] {bestMove[0], bestMove[1], alpha};
        return new int[] {bestMove[0], bestMove[1], beta};
    }

    //Beta Pruning negamax function
    int[] negamaxBetaP(int depth, int player, int alpha, int beta) {

        //Create list for all possible moves on board
        List<int[]> moves = new ArrayList<int[]>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (cells[i][j] == 0) {
                    moves.add(new int[] {i, j});
                }
            }
        }

        //init return variables
        int bestValue = alpha;
        int[] bestMove = {-1,-1};


        //calculate score for depth = 0
        if(moves.isEmpty() || depth == 0){
            bestValue = 0;
            for(int i = 0; i < linesToEvaluate.size(); i++) {
                bestValue += heuristicEvaluation(linesToEvaluate.get(i), player);
            }
            return new int[] {bestMove[0], bestMove[1], bestValue};
            //Log.i("ERROR", "" + bestValue); //TODO remove DEBug stuff
        }
        else //recursive depth
        {
            int[] temp = {-1,-1};
            for(int i = 0; i < moves.size(); i++){ //try each move
                temp = moves.get(i);
                cells[temp[0]][temp[1]] = player;

                bestValue = -negamaxBetaP(depth - 1, -playerOwns, -beta, -alpha)[2]; //reverse beta/alpha an negate
                if (bestValue > alpha) {
                    alpha = bestValue;
                    bestMove = temp;
                }

                // Reset Move
                cells[temp[0]][temp[1]] = 0;

                if (alpha >= beta)
                {
                    break;
                }
            }

        }

        return new int[] {bestMove[0], bestMove[1], alpha};

    }



    /*Heuristic Evaluation

    Using Resources, posted at top
    Dong Xiang, "Solve Tic Tac Toe with the MiniMax algorithm" @ http://www.codeproject.com/Articles/43622/Solve-Tic-Tac-Toe-with-the-MiniMax-algorithm.

    For each row, if there are both X and O, then the score for the row is 0.
    If the whole row is empty, then the score is 1.
    If there is only one X, then the score is 10.
    If there are two Xs, then the score is 100.
    If there are 3 Xs, then the score is 1000, and the winner is Player X. //ignoring this step because Game determines win (I set that up first)
    For Player O, the score is negative.
    Player X tries to maximize the score.
    Player O tries to minimize the score.
    If the current turn is for Player X, then the score of Player X has more advantage. I gave the advantage rate as 3.*/

    private int heuristicEvaluation(int[] line, int player){

        int playerCount = 0;
        int computerCount = 0;

        for(int i = 0, j = 1; i < 5; i+=2,j+=2)
        {
            if(cells[line[i]][line[j]] == computerOwns){
                computerCount++;
            }else if(cells[line[i]][line[j]] == playerOwns){
                playerCount++;
            }

        }

        //possible gameover if statement here if either player has 3+ score by now
        //redundant, handled in main

        //Calculate score
        int notYourTurn = 1;
        if (computerCount == 0)//if no computer nodes and player owns some in line
        {
            if(playerCount != player)
                notYourTurn *= -1;
            return (int)Math.pow(10, playerCount)*notYourTurn;
        }
        else if (playerCount == 0)//if no player nodes and computer owns some in line
        {
            if(computerCount != player)
                notYourTurn *= -1;
            return -(int)Math.pow(10, computerCount)*notYourTurn;
        }
        return 0;
    }
    
}
