package com.example.m100266177.tic_tac_toe_ai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Random;

/**
 * Created by 100266177 on 20/10/2015.
 */
public class Game extends Activity {

    //row major
    //0 for nothing, -1 for computer, 1 for player
    int[][] board = new int[3][3];
    boolean playersTurn = true;
    AI AIPlayer;

    int playerOwns = 1;
    int computerOwns = -1;
    String playerLetter;
    String computerLetter;

    final static String FIRST_PLAYER = "X";
    final static String SECOND_PLAYER = "O";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game);

        //init board, row major
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                board[i][j] = 0;
            }
        }

        //init AI
        AIPlayer = new AI(board);


        //Recieve isPlayer "X" or "O" intent
        Intent intent = getIntent();
        String letter = intent.getStringExtra(MainMenu.GAME_START);
        if(letter.equals("X") == true){
            playerLetter = FIRST_PLAYER; computerLetter = SECOND_PLAYER;
            playersTurn = true;
        }else{
            computerLetter = FIRST_PLAYER; playerLetter = SECOND_PLAYER;
            playersTurn = false;
            isComputersTurn();
        }

    }



    //Button handlers for tic tac toe grid
    public void handle(View view)
    {
        if(playersTurn == true) {
            Button button = (Button) findViewById(view.getId());
            String name = button.getResources().getResourceName(view.getId());
            //Parse name and convert last two characters to int, set array to checked
            String number = name.length() > 2 ? name.substring(name.length() - 2) : name;
            int temp = Integer.parseInt(number);
            int row = temp/10;
            int col = temp%10;
            if(board[row][col] == 0) {//TODO separate function? so ai can check too? void(row, col, currentPlayer)
                board[row][col] = playerOwns;
                button.setText(playerLetter);//TODO change based on what player is
                checkWin(playerOwns);
            }
            playersTurn = false;
        }
        isComputersTurn();
    }

    //public void isPlayersTurn()
    //{
//
    //}

    public void isComputersTurn()
    {
        if(playersTurn == false) {
            //TODO Do AI actions
            int[] move = AIPlayer.generateMove(board);
            String buttonName = "button_" + move[0] + move[1];
            int id = this.getResources().getIdentifier(buttonName, "id", this.getPackageName());
            //Log.i("ERROR", buttonName);//TODO remove debug stuff
            //Log.i("ERROR", "" + id);
            Button compButton = (Button) findViewById(id);
            if (move[0] != -1)
                if (board[move[0]][move[1]] == 0) {
                    board[move[0]][move[1]] = computerOwns;
                    compButton.setText(computerLetter);//TODO change based on what player is
                }
            checkWin(computerOwns);
            playersTurn = true;
        }
    }


    //close game button
    public void gameCloseButtonHandle(View view){
        win(0);
    }

    //Send winner result back to main menu, end game
    public void win(int currentPlayer){
        String result;
        if(currentPlayer == playerOwns)
            result = "You Win";
        else if(currentPlayer == computerOwns)
            result = "AI Wins";
        else
            result = "Draw";

        Intent resultIntent = new Intent(Intent.ACTION_PICK);
        resultIntent.putExtra(MainMenu.GAME_START, result);
        setResult(RESULT_OK, resultIntent);

        finish();
    }

    //Check all win conditions for current player, call after player/AI move
    public void checkWin(int currentPlayer){
        int[][] cells = board;
        //rows and col
        for(int i = 0; i < 3; i++)
        {
            if (cells[i][0] == currentPlayer)
                if (cells[i][1] == currentPlayer)
                    if (cells[i][2] == currentPlayer)
                        win(currentPlayer);
        }
        for(int i = 0; i < 3; i++)
        {
            if (cells[0][i] == currentPlayer)
                if (cells[1][i] == currentPlayer)
                    if (cells[2][i] == currentPlayer)
                        win(currentPlayer);
        }

        //diagnols
        if((cells[0][0] == currentPlayer)&&(cells[1][1] == currentPlayer)&&(cells[2][2] == currentPlayer))
            win(currentPlayer);
        if((cells[2][0] == currentPlayer)&&(cells[1][1] == currentPlayer)&&(cells[0][2] == currentPlayer))
            win(currentPlayer);
        //checkdraw
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                if(cells[i][j] == 0)
                    return;
            }
        }
        win(0);
    }

    //public class AIPlayer{
//
    //    protected int[][] cells;
//
    //    public AIPlayer(int[][] board) {
    //        cells = board;
    //    }
//
    //    int[] move();
    //}
}
