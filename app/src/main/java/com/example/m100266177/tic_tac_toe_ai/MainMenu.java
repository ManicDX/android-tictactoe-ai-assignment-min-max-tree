package com.example.m100266177.tic_tac_toe_ai;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

public class MainMenu extends Activity {

    public final static String GAME_START = "GAME";
    public static int GAME_CODE = 1010101;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void playButtonHandle(View view){

        Intent intent = new Intent(this, Game.class);
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
        String playerLetter = "O";
        if(checkBox.isChecked() == true)
            playerLetter = "X";
        intent.putExtra(GAME_START, playerLetter);
        startActivityForResult(intent, GAME_CODE);
        //

    }

    //@Override
    public void closeButtonHandle(View view){
        finish();
    }

    @Override
    public void onActivityResult(int requestCode, int responseCode, Intent resultIntent){
        super.onActivityResult(requestCode, responseCode, resultIntent);

        //Get result of either yes or no from askQuestion, and add to yes/no array
        if(responseCode == RESULT_OK){

            String inc = resultIntent.getStringExtra(GAME_START);

            TextView textView = (TextView) findViewById(R.id.winnerTextView);
            textView.setText(String.valueOf(inc));
        }
    }
}
